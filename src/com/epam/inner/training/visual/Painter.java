package com.epam.inner.training.visual;

import java.awt.*;

/**
 * Класс Painter с полем <b>graphics</b>.
 * Painter отвечает за рисование нужных в приложении
 * объектов.
 * @version 2.1
 * @autor Asadchy Raman
 */
public class Painter {

    /**
     * Инструмент для рисования
     */
    private Graphics graphics;

    /**
     * Создает <code>Painter</code> объект.
     * @param graphics  инструмет для рисования
     **/
    public Painter(Graphics graphics) {
        this.graphics = graphics;
    }

    /**
     * Метод отрисовывает людей в приложении таких как :
     * <code>Client</code> и <code>Cashier</code>
     * @param posX  Позиция x
     * @param posY  Позиция y
     * @param size  Размер объекта
     * @param color Цвет объекта
     */
    public void paintHuman(int posX, int posY, int size, Color color) {
        this.graphics.setColor(color);
        this.graphics.fillArc(posX, posY, size, size, 0, 360);
        this.graphics.setColor(Color.WHITE);
        this.graphics.drawArc(posX + 1, posY + 1, size, size, 120, 30);
    }

    /**
     * Метод удаления людей в приложении таких как :
     * <code>Client</code> и <code>Cashier</code>.
     * Нужо для реализации движения
     * @param posX  Позиция x
     * @param posY  Позиция y
     * @param size  Размер объекта
     * @param color Цвет объекта
     */
    public void deleteHuman(int posX, int posY, int size, Color color) {
        try {
            Thread.sleep(30);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //удаление шарика
        this.graphics.setColor(color);
        this.graphics.fillArc(posX, posY, size, size, 0, 360);
    }

    /**
     * Метод отрисовки сообщений.
     * @param posX     Позиция x
     * @param posY     Позиция y
     * @param message  Текст сообщения
     * @param color    Цвет фона
     * @param textSize Размер текста
     * @throws InterruptedException
     */
    public void paintMessage(int posX, int posY, String message, Color color, int textSize) throws InterruptedException {
        this.graphics.setFont(new Font("TimesRoman", Font.BOLD, textSize));
        this.graphics.setColor(Color.MAGENTA);
        this.graphics.drawString(message, posX + 15, posY - 30);
        this.graphics.drawLine(posX + 20, posY + 10, posX + 10, posY - 30);
        this.graphics.drawLine(posX + 10, posY - 30, posX + 60, posY - 30);
        Thread.sleep(500);
        this.graphics.setColor(color);
        this.graphics.drawString(message, posX + 15, posY - 30);
        this.graphics.drawLine(posX + 20, posY + 10, posX + 10, posY - 30);
        this.graphics.drawLine(posX + 10, posY - 30, posX + 60, posY - 30);

    }

    /**
     * Метод отрисовки кассы
     * @param posX   Позиция x
     * @param posY   Позиция y
     * @param width  Ширина кассы
     * @param heigth Высота кассы
     */
    public void paintCashBox(int posX, int posY, int width, int heigth) {
        this.graphics.setColor(Color.BLACK);
        this.graphics.drawRect(posX, posY, width, heigth);
        this.graphics.fillRect(posX, posY, width / 2, heigth / 4);
        this.graphics.fillRect(posX, posY + heigth - 40, width / 2, heigth / 2);
        this.graphics.drawLine(posX + width / 2, posY + 20, posX + width / 2, posY + 40);
        this.graphics.setColor(Color.BLACK);
    }


}
