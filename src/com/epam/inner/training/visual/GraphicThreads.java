package com.epam.inner.training.visual;

import com.epam.inner.training.bean.cashBox.CashBox;
import com.epam.inner.training.bean.cashBox.QueueClients;
import com.epam.inner.training.bean.client.Client;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

/**
 * Класс GraphicThreads с полями <b>panel</b> and <b>list</b>.
 * @version 2.1
 * @autor Asadchy Raman
 */
public class GraphicThreads extends JFrame {

    /**
     * Рабочая панель
     */
    private JPanel panel;

    /**
     * Список кассе
     */
    private LinkedList<CashBox> list;


    /**
     * Создает <code>GraphicThreads</code> объект.
     * При инициализации объекта класса GraphicThreads
     * задаются размеры основного фрейма, а также рабочей панели.
     * Инициализируются вспомогательные кнопки : добавить клиента,
     * добавить кассу, обновить статистику.
     * @param list   Список касс
     * @param jPanel Рабочая панель
     **/
    public GraphicThreads(LinkedList<CashBox> list, JPanel jPanel) {
        this.panel = jPanel;
        this.list = list;
        Dimension sSize = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(sSize);
        Container contentPane = getContentPane();
        contentPane.setLayout(null);
        JButton buttonAddClient = new JButton("Добавить клиента");
        buttonAddClient.setBounds(700, 40, 160, 20);
        JButton buttonAddCashBox = new JButton("Добавить кассу");
        buttonAddCashBox.setBounds(500, 40, 160, 20);
        JButton updateStatistics = new JButton("Обновить статистику");
        updateStatistics.setBounds(300, 40, 160, 20);
        contentPane.add(buttonAddClient);
        contentPane.add(buttonAddCashBox);
        contentPane.add(updateStatistics);
        panel.setBounds(300, 100, 1200, 600);
        panel.setBackground(Color.WHITE);
        contentPane.add(panel);

        buttonAddClient.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ev) {
                new Client(panel, list, new Random());
            }
        });

        buttonAddCashBox.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ev) {
                list.add(new CashBox(panel, list, new Painter(panel.getGraphics()), new QueueClients()));
                repaint();
            }
        });

        updateStatistics.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ev) {
                repaint();
            }
        });
    }


    /**
     * Метод выводит в фрейм количество касс и информацию
     * о прибыли каждой кассы
     * @param graphics  объект для рисования.
     */
    public void paint(Graphics graphics) {
        super.paint(graphics);
        graphics.drawString("Количество касс: " + list.size(), 65, 100);
        for (int i = 0; i < list.size(); i++) {
            graphics.drawString("Касса №" + (i + 1) + " Прибыль : " + list.get(i).getSumMoney(), 65, 120 + i * 15);
        }
    }

    /**
     * Метод запускает поток, который создает клиентов каждые
     * 5 секунд.
     */
    public void creatorClient() {
        new Thread() {
            @Override
            public void run() {
                Random random = new Random();
                while (true) {
                    try {
                        sleep(random.nextInt(5)* 1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    new Client(panel, list, new Random());
                }
            }
        }.start();
    }

    /**
     * Метод создает заданное количество объектов CashBox
     * @param size  Количество касс которое хотим создать
     *              при старте приложения.
     */
    public void creatorCashBox(int size) {
        for (int i = 0; i < size; i++) {
            list.add(new CashBox(panel, list, new Painter(panel.getGraphics()), new QueueClients()));
        }
    }
}
