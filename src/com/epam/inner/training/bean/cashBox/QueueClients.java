package com.epam.inner.training.bean.cashBox;

import com.epam.inner.training.bean.client.Client;

import java.util.LinkedList;

public class QueueClients {

    private final LinkedList<Client> clients;

    public QueueClients() {
        clients = new LinkedList<>();
    }

    public LinkedList<Client> getClients() {
        return clients;
    }

    public void addClient(Client client) {
        clients.add(client);
    }

    public void removeFirstClient() {
        clients.removeFirst();
    }

    public Client getLastClient() {
        return clients.getLast();
    }

    public Client getFirstClient() {
        return clients.getFirst();
    }

    public boolean isEmpty() {
        return clients.isEmpty();
    }

    public int getSize() {
        return clients.size();
    }

    public Client get(int index) {
        return clients.get(index);
    }

    public int getSumProductsInQueue() {
        int sumProducts = 0;
        for (Client client : clients) {
            sumProducts += client.getProducts();
        }
        return sumProducts;
    }
}
