package com.epam.inner.training.bean.cashBox;

import com.epam.inner.training.visual.Painter;

import javax.swing.*;
import java.util.LinkedList;

/**
 * Класс CashBox
 * @version 2.1
 * @autor Asadchy Raman
 */
public class CashBox implements Runnable {

    /**
     * Рабочая панель
     */
    private JPanel panel;

    /**
     * Инструмент для рисования
     */
    private Painter painter;

    /**
     * Позиция по оси Ox
     */
    private int posX;

    /**
     * Позиция по оси Oy
     */
    private int posY;

    /**
     * Работает ли в данный момент времени
     */
    private boolean work = true;

    /**
     * Очередь клиентов
     */
    private QueueClients queueClients;

    /**
     * Количество клиентов которые обслужены
     */
    private int clientsWereServed;

    /**
     * Прибыль
     */
    private int sumMoney;

    /**
     * Кассир определнный в кассе
     */
    private Cashier cashier;

    /**
     * Ширина кассы
     */
    public static final int WIDTH = 80;

    /**
     * Высота кассы
     */
    public static final int HEIGHT = 80;

    /**
     * Позиция кассира
     */
    public static final int POSITION_CASHIER_Y = HEIGHT / 2 + 10;

    /**
     * Расстояние между соседними кассами
     */
    private static final int DISTANCE_BETWEEN_CASH_BOX = 100;

    /**
     * Стартовая позиция кассы по Ox
     */
    private static final int START_POSITION_CASH_BOX_X = 10;

    /**
     * Стартовая позиция кассы по Oy
     */
    private static final int START_POSITION_CASH_BOX_Y = 30;

    /**
     * Время для обслуживания одного продукта
     */
    private static final int TIME_TO_SERVE_ONE_PRODUCTS = 5000; // milli second

    /**
     * Создает <code>CashBox</code> объект.
     * При инициализации объекта класса CashBox
     * задаются : начальная позиция кассы по Ox и
     * Oy.
     * @param panel         Рабочая панель.
     * @param list          Список касс.
     * @param painter       Инструмент для рисования
     * @param queueClients  Очередь клиентов
     **/

    public CashBox(JPanel panel, LinkedList<CashBox> list, Painter painter, QueueClients queueClients) {
        this.painter = painter;
        this.panel = panel;
        this.queueClients = queueClients;
        if (!list.isEmpty()) {
            posX = list.getLast().posX + DISTANCE_BETWEEN_CASH_BOX;
        } else {
            posX = START_POSITION_CASH_BOX_X;
        }
        posY = START_POSITION_CASH_BOX_Y;
        this.cashier = new Cashier(panel, this);
        new Thread(this).start();

    }

    public QueueClients getQueueClients() {
        return queueClients;
    }

    public void setQueueClients(QueueClients queueClients) {
        this.queueClients = queueClients;
    }

    public JPanel getPanel() {
        return panel;
    }

    public void setPanel(JPanel panel) {
        this.panel = panel;
    }

    public boolean isWork() {
        return work;
    }

    public void setWork(boolean work) {
        this.work = work;
    }

    public int getSumMoney() {
        return sumMoney;
    }

    public void setSumMoney(int sumMoney) {
        this.sumMoney = sumMoney;
    }


    public int getPosX() {
        return posX;
    }

    public void setPosX(int posX) {
        this.posX = posX;
    }

    public int getPosY() {
        return posY;
    }

    public void setPosY(int posY) {
        this.posY = posY;
    }

    @Override
    public void run() {
        new Thread() {
            public void run() {
                while (true) {
                    painter.paintCashBox(posX, posY, WIDTH, HEIGHT);
                }
            }
        }.start();

        while (true) {
            if (clientsWereServed == 5) {
                goToBreak();
            }
            while (queueClients.isEmpty()) {
                Thread.yield();
            }
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (queueClients.getFirstClient().getPosY() == HEIGHT - posY) {
                serveCustomer();
            }
        }

    }

    /**
     * Метод для определения перерыва у кассира
     */
    private void goToBreak() {
        this.work = false;
        this.cashier.setWork(false);
        while (!this.cashier.isWork()) {
            Thread.yield();
        }
        this.work = true;
        clientsWereServed = 0;
    }


    /**
     * Метод обслуживания клиентов, забираем все
     * деньги имеющиеся у клиентов.
     */
    private void serveCustomer() {
        try {
            Thread.sleep(queueClients.getFirstClient().getProducts() * TIME_TO_SERVE_ONE_PRODUCTS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        sumMoney += queueClients.getFirstClient().getMoney();
        queueClients.getFirstClient().setMoney(0);
        queueClients.removeFirstClient();
        clientsWereServed++;
    }
}
