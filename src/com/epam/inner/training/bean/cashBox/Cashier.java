package com.epam.inner.training.bean.cashBox;

import com.epam.inner.training.visual.Painter;

import javax.swing.*;
import java.awt.*;

/**
 * Класс Cashier.
 * Кассир, стоит за кассой
 * @version 2.1
 * @autor Asadchy Raman
 */
public class Cashier implements Runnable {

    /**
     * Рабочая панель
     */
    private JPanel panel;

    /**
     * Позиция по оси Ox
     */
    private int posX;

    /**
     * Позиция по оси Oy
     */
    private int posY;

    /**
     * Работает ли в данный момент времени
     */
    private boolean work = true;


    /**
     * Размер кассира
     */
    private final int SIZE = 20;

    /**
     * Позиция по оси Oy для перерыва.
     */
    private static final int POSITION_FOR_BREAK_Y = -50;

    /**
     * Скорость передвижения кассира
     */
    private static final int SPEED = 2;


    public boolean isWork() {
        return work;
    }

    public void setWork(boolean work) {
        this.work = work;
    }

    /**
     * Создает <code>Cashier</code> объект.
     * При инициализации объекта класса Cashier
     * задаются начальные координаты posX и posY,
     * а так же инициализируется рабочая панель
     * @param panel   Рабочая панель
     * @param cashBox Касса в которую определили кассира
     **/
    public Cashier(JPanel panel, CashBox cashBox) {
        this.panel = panel;
        this.posX = (int) (cashBox.getPosX() + CashBox.HEIGHT / 4);
        this.posY = (int) (CashBox.POSITION_CASHIER_Y);
        new Thread(this).start();
    }

    public int getPosX() {
        return posX;
    }

    public void setPosX(int posX) {
        this.posX = posX;
    }

    public int getPosY() {
        return posY;
    }

    public void setPosY(int posY) {
        this.posY = posY;
    }

    /**
     * Метод определяет передвижения кассира по Oy.
     * От точки его работы, до точки перерыва.
     * @param painter
     * @throws InterruptedException
     */
    public void changePositionY(Painter painter) throws InterruptedException {
        Thread.sleep(1800);
        while (posY != POSITION_FOR_BREAK_Y) {
            painter.deleteHuman(posX, posY, SIZE, panel.getBackground());
            posY += (int) (SPEED * Math.sin(Math.toRadians(-90)));
            painter.paintHuman(posX, posY, SIZE, Color.MAGENTA);
        }
        Thread.sleep(15000);
        while (posY != CashBox.POSITION_CASHIER_Y) {
            painter.deleteHuman(posX, posY, SIZE, panel.getBackground());
            posY += (int) (SPEED * Math.sin(Math.toRadians(90)));
            painter.paintHuman(posX, posY, SIZE, Color.MAGENTA);
        }
        this.work = true;
    }

    @Override
    public void run() {
        Painter painter = new Painter(panel.getGraphics());
        while (true) {
            painter.paintHuman(posX, posY, SIZE, Color.MAGENTA);
            if (!this.work) {
                try {
                    painter.paintMessage(posX, posY, "Перерыв!", panel.getBackground(), 20);
                    changePositionY(painter);
                    painter.paintMessage(posX, posY, "Открыто!", panel.getBackground(), 20);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
