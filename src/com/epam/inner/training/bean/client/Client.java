package com.epam.inner.training.bean.client;

import com.epam.inner.training.bean.cashBox.CashBox;
import com.epam.inner.training.visual.Painter;

import javax.swing.*;
import java.awt.*;
import java.util.LinkedList;

import java.util.Random;

public class Client implements Runnable {

    /**
     * Рабочая панель
     */
    private final JPanel panel;

    /**
     * Позиция по оси Ox
     */
    private int posX;

    /**
     * Позиция по оси Oy
     */
    private int posY;

    /**
     * Количество денег у клиента
     */
    private double money = 40;

    /**
     * Количество продуктов
     */
    private int products;

    /**
     * Список касс
     */
    private LinkedList<CashBox> list;

    /**
     * Размер клиента
     */
    private static final int BALL_SIZE = 20;

    /**
     * Скорость клиента
     */
    private static final int SPEED = 10;

    /**
     * Дистанция которую должен держать клент
     * между другим клиентом.
     */
    private static final int DISTANCE_BETWEEN_CLIENT = 30;

    /**
     * Создает <code>Client</code> объект.
     * При инициализации объекта класса Client
     * задаются начальные координаты posX и posY,
     * а так же инициализируется рабочая панель,
     * список касс и по средствам рандома
     * определяется количество продуктов
     * @param panel  Рабочая панель
     * @param list   Список касс
     * @param random Объект класса Random
     */
    public Client(JPanel panel, LinkedList<CashBox> list, Random random) {
        this.panel = panel;
        this.list = list;
        this.products = random.nextInt(10);
        this.posX = (int) ((panel.getWidth() - BALL_SIZE) * 0.5 - ((panel.getWidth() - BALL_SIZE) * 0.5) % 10);
        this.posY = (int) ((panel.getHeight() - BALL_SIZE) * 0.9 - ((panel.getHeight() - BALL_SIZE) * 0.9) % 10);
        new Thread(this).start();
    }

    public int getProducts() {
        return products;
    }

    public void setProducts(int products) {
        this.products = products;
    }

    public int getPosX() {
        return posX;
    }

    public void setPosX(int posX) {
        this.posX = posX;
    }

    public int getPosY() {
        return posY;
    }

    public void setPosY(int posY) {
        this.posY = posY;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    @Override
    public void run() {
        Painter painter = new Painter(panel.getGraphics());
        CashBox cashBox = chooseCashBoxWhereLeastQueue();
        cashBox.getQueueClients().addClient(this);
        Client lastClientInQueue = null;
        if (cashBox.getQueueClients().getSize() > 1) {
            lastClientInQueue = cashBox.getQueueClients().get(cashBox.getQueueClients().getSize() - 2);
        }
        changePositionX(cashBox, painter);

        while (this.money != 0) {
            try {
                changePositionY(cashBox, lastClientInQueue, painter);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            painter.paintHuman(this.posX, this.posY, BALL_SIZE, Color.green);
        }
        painter.deleteHuman(posX, posY, BALL_SIZE, panel.getBackground());
    }

    /**
     * Метод определяет передвижения клиента по Ox.
     * @param cashBox  Касса которую выбрал клиент
     * @param painter  Инструмент для рисования
     */
    private void changePositionX(CashBox cashBox, Painter painter) {
        double angleMove = (cashBox.getPosX() + CashBox.HEIGHT - BALL_SIZE - 10 >= this.panel.getWidth() / 2)
                ? Math.toRadians(0.0)
                : Math.toRadians(180.0);

        while (this.posX != cashBox.getPosX() + CashBox.HEIGHT - BALL_SIZE - 10) {
            this.posX += (int) (SPEED * Math.cos(angleMove));
            painter.paintHuman(this.posX, this.posY, BALL_SIZE, Color.green);
            painter.deleteHuman(posX, posY, BALL_SIZE, panel.getBackground());
        }
    }

    /**
     * Метод определяет передвижения клиента по Oy.
     * @param cashBox            Касса которую выбрал клиент
     * @param lastClientInQueue  Последний клиент в очереди
     * @param painter            Инструмент для рисования
     * @throws InterruptedException
     */
    private void changePositionY(CashBox cashBox, Client lastClientInQueue,
                                 Painter painter) throws InterruptedException {
        while (this.posY != CashBox.HEIGHT - cashBox.getPosY()) {
            if (cashBox.getQueueClients().getFirstClient().equals(this)
                    || this.posY != lastClientInQueue.getPosY() + DISTANCE_BETWEEN_CLIENT) {
                painter.deleteHuman(posX, posY, BALL_SIZE, panel.getBackground());
                this.posY += (int) (SPEED * Math.sin(Math.toRadians(-90)));
            } else {
                if (!cashBox.isWork()) {
                    painter.paintMessage(posX, posY, "Орёт", panel.getBackground(), 15);
                }
                Thread.sleep(100);
            }
            painter.paintHuman(this.posX, this.posY, BALL_SIZE, Color.green);
        }
    }

    /**
     * Метод выбора кассы клиентом где меньше всего очередь
     */
    private CashBox chooseCashBoxWhereLeastQueue() {
        int size = this.list.get(0).getQueueClients().getSize();
        CashBox chooseCashBox = this.list.get(0);
        for (CashBox cashBox : this.list) {
            if (size >= cashBox.getQueueClients().getSize() && chooseCashBox.isWork()) {
                chooseCashBox = cashBox;
                size = cashBox.getQueueClients().getSize();
            } else if (!chooseCashBox.isWork()) {
                chooseCashBox = chooseCashBoxWhereLeastProducts();
                break;
            }
        }

        return chooseCashBox;
    }

    /**
     * Метод выбора кассы клиентом где меньше всего продуктов в очереди
     */
    private CashBox chooseCashBoxWhereLeastProducts() {
        CashBox chooseCashBox = list.get(0);
        int sumProducts = list.getLast().getQueueClients().getSumProductsInQueue();
        for (CashBox cashBox : list) {
            if (sumProducts >= cashBox.getQueueClients().getSumProductsInQueue()) {
                chooseCashBox = cashBox;
                sumProducts = cashBox.getQueueClients().getSumProductsInQueue();
            }
        }
        return chooseCashBox;
    }
}
