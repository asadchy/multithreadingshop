package com.epam.inner.training.runner;


import com.epam.inner.training.visual.GraphicThreads;

import javax.swing.*;

import java.util.LinkedList;

import static java.awt.Frame.MAXIMIZED_BOTH;


public class Runner {
    public static void main(String[] args) {
        GraphicThreads frame = new GraphicThreads(new LinkedList<>(), new JPanel());
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setVisible(true);
        frame.setExtendedState(MAXIMIZED_BOTH);
        frame.creatorCashBox(2);
        frame.creatorClient();
    }
}
